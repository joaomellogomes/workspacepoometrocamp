	package br.com.controleestoque.model;

/*
 * @author Jo�o Vitor de Mello Gomes dos Reis
 * @author Carlos Eduardo Trabuco da Silva
 * @author Luiz Fernando Silva C de Souza
 */
public class Estoque {

	//atributos
	private String nome;
	private int qtdAtual;
	private int qtdMinima;
	
	//	construtores
	public Estoque() {
		this.nome = "";
		this.qtdAtual = 0;
		this.qtdAtual = 0;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getQtdAtual() {
		return qtdAtual;
	}

	public void setQtdAtual(int qtdAtual) {
		this.qtdAtual = qtdAtual >= 0 ? qtdAtual : this.qtdAtual;
	}

	public int getQtdMinima() {
		return qtdMinima;
	}

	public void setQtdMinima(int qtdMinima) {
		this.qtdMinima = qtdMinima >= 0 ? qtdMinima : this.qtdMinima;
	}
	
	public void repor(int qtd) {
		this.qtdAtual += qtd;
	}
	
	public void darBaixa(int qtd) {
		this.qtdAtual -= qtd;
	}
	
	public String mostra() {
		return "Nome do Produto: " + this.nome +
				"\nQuantidade M�nima: " + this.qtdMinima +
				"\nQuantidade Atual: " + this.qtdAtual;
	}
	
	public Boolean precisaRepor() {
		return this.qtdAtual <= qtdMinima ? true : false;
	}
	
}
