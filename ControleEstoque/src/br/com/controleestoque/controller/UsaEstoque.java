package br.com.controleestoque.controller;

import javax.swing.JOptionPane;

import br.com.controleestoque.model.Estoque;

public class UsaEstoque {

	public static void main(String[] args) {

		Estoque estoque1 = new Estoque();
		Estoque estoque2 = new Estoque();
		Estoque estoque3 = new Estoque();
		
		estoque1.setNome("Impressora Jato de Tinta");
		estoque1.setQtdAtual(13);
		estoque1.setQtdMinima(6);
		
		estoque2.setNome("Monitor LCD 17 Polegadas");
		estoque2.setQtdAtual(11);
		estoque2.setQtdMinima(13);
		
		estoque3.setNome("Mouse �ptico");
		estoque3.setQtdAtual(6);
		estoque3.setQtdMinima(2);
		
		for (int i = 0; i < 5; i++) {
			estoque1.darBaixa(1);		
		}
		
		for (int i = 0; i < 7; i++) {
			estoque2.repor(1);			
		}
		
		for (int i = 0; i < 4; i++) {
			estoque3.darBaixa(1);		
		}
		
		JOptionPane.showMessageDialog(null, "ESTOQUE\n\n" + estoque1.mostra()
											+ "\n\nPRECISA REPOR?: " + 
												(estoque1.precisaRepor() ? "SIM" : "N�O"), "Estoque 1", JOptionPane.INFORMATION_MESSAGE);
		
		JOptionPane.showMessageDialog(null, "ESTOQUE\n\n" + estoque2.mostra()
											+ "\n\nPRECISA REPOR?: " + 
											(estoque2.precisaRepor() ? "SIM" : "N�O"), "Estoque 2", JOptionPane.INFORMATION_MESSAGE);
		
		JOptionPane.showMessageDialog(null, "ESTOQUE\n\n" + estoque3.mostra()
											+ "\n\nPRECISA REPOR?: " + 
											(estoque3.precisaRepor() ? "SIM" : "N�O"), "Estoque 3", JOptionPane.INFORMATION_MESSAGE);

	}

}
