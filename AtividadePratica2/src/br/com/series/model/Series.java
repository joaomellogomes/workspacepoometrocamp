package br.com.series.model;

public class Series {
	
	//Atributos
	private int i1;
	private int i2;
	private int passo;
	
	public void setI1(int i1) {
		this.i1 = i1;
	}
	
	public void setI2(int i2) {
		this.i2 = i2;
	}
	
	public void setPasso(int passo) {
		this.passo = passo < 1 ? 1 : passo;
	}

	public String mostraSerie() {		
		
		String serie = "";
		
		if(this.i1 < this.i2) {	
			
			for (int i = this.i1; i <= this.i2 ; i+= this.passo) {
				serie += i + ";";
			}
			
		}else {
			
			for (int i = this.i1; i >= this.i2 ; i -= this.passo) {
				serie += i + ";";
			}
			
		}
		
		serie = serie.substring(0, serie.length() - 1);
		
		return serie;
	}
	

}
