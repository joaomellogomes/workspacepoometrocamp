package br.com.series.controller;

import javax.swing.JOptionPane;

import br.com.series.model.Series;

public class TestaSeries {

	public static void main(String[] args) {
	
		Series s1 = new Series();
		Series s2 = new Series();
		Series s3 = new Series();
		
		s1.setI1(1);
		s1.setI2(10);
		s1.setPasso(1);
		
		s2.setI1(10);
		s2.setI2(0);
		s2.setPasso(2);
		
		s3.setI1(-5);
		s3.setI2(5);
		s3.setPasso(1);
		
		JOptionPane.showMessageDialog(null, "Resultado mostra s�ries: \n" + s1.mostraSerie(), "Mostra S�ries 1", JOptionPane.PLAIN_MESSAGE);
		JOptionPane.showMessageDialog(null, "Resultado mostra s�ries: \n" + s2.mostraSerie(), "Mostra S�ries 2", JOptionPane.PLAIN_MESSAGE);
		JOptionPane.showMessageDialog(null, "Resultado mostra s�ries: \n" + s3.mostraSerie(), "Mostra S�ries 3", JOptionPane.PLAIN_MESSAGE);

	}

}
